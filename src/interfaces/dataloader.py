from abc import ABC, abstractmethod



# Interfaces pour l'inversion de dépendance et la ségrégation d'interface
class DataLoader(ABC):
    """
    Classe modèle pour charger des données. Les classes dérivées doivent implémenter 'load_data'.
    """

    @abstractmethod
    def load_data(self):
        """
        Charge les données. À définir dans chaque sous-classe.
        """
        pass


class DataExporter(ABC):
    """
        Classe modèle pour sauvegarder des données. Les classes dérivées doivent implémenter 'export_data'.
    """

    @abstractmethod
    def export_data(self, data, file_name):
        pass
