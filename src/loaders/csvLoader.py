import pandas as pd
from src.interfaces.dataloader import DataLoader


class CSVLoader(DataLoader):
    def __init__(self, file_path):
        self.file_path = file_path

    def load_data(self):
        return pd.read_csv(self.file_path)
